﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema


Namespace ControlRemissions.JimyDiaz.Model
    Public Class Remission
        #Region "Campos"
        Private _NumberOrder As integer
        Private _DateOfSubmission as DateTime
        Private _Direction As String
        Private _TypeOfRemission As Integer
        Private _AgentNumber as Integer
        Private _LicensePlate As String
#End Region

        #Region "Propiedades"
        Public  Overridable Property Agent() As Agent
        Public  Overridable Property TypeOfReferral() As TypeOfReferral
        Public Overridable Property Vehicle() As Vehicle
        <Key()>
        Property NumberOder() As Integer
        Get
                Return _NumberOrder
        End Get
            Set(value As Integer)
                _NumberOrder = value
            End Set
        End Property

        Public  Property  DateOfSubmission() As Date
        Get
                Return _DateOfSubmission
        End Get
            Set(value As DateTime)
                _DateOfSubmission = value
            End Set
        End Property
        <ForeignKey("TypeOfReferral")>
        Public Property TypeOfRemission As Integer
        Get
                Return _TypeOfRemission
        End Get
            Set(value As Integer)
                _TypeOfRemission = value
            End Set
        End Property

        Public  Property Direction() As String
        Get
                Return _Direction
        End Get
            Set(value As String)
                _Direction =  value
            End Set
        End Property
        <ForeignKey("Agent")>
        Public Property AgentNumber As Integer
        Get
                Return _AgentNumber
        End Get
            Set(value As Integer)
                _AgentNumber = value
            End Set
        End Property

 
        Public Property LicensePlate() As String
        Get
                Return _LicensePlate
        End Get
            Set(value As String)
                _LicensePlate = value
            End Set
        End Property
#End Region
    End Class

End Namespace

