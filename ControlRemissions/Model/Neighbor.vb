﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema


Namespace  ControlRemissions.JimyDiaz.Model
    
    Public Class Neighbor
        #Region "Campos"
        Private _NIT as String
        Private _DPI as Long
        Private _FirstName As String
        Private  _LastName As String
        Private _Direction as String
        Private _Municipality As String
        Private  _CodePostal as Integer   
        Private _Phone as Integer

#End Region
        
        #Region "Propiedades"
        Public Overridable Property Vehicles() As ICollection(of Vehicle)


        <Key()>
        Public Property  NIT() As String
        Get
                Return  _NIT
        End Get
            Set(value As String)
                _NIT = value
            End Set
        End Property

        Public  Property DPI() As Integer
        Get
                Return _DPI
        End Get
            Set(value As Integer)
                _DPI = value
            End Set
        End Property

        PUblic Property FirstName() As String
        Get
                Return _FirstName
        End Get
            Set(value As String)
                _FirstName = value
            End Set
        End Property


        Public  Property LastName() As String
        Get
                Return _LastName
        End Get
            Set(value As String)
                _LastName = value
            End Set
        End Property

        Public Property  Direction() As String
        Get
                Return _Direction
        End Get
            Set(value As String)
                _Direction = value
            End Set
        End Property

        Public  Property  Municipality() As String
        Get
                Return _Municipality
        End Get
            Set(value As String)
                _Municipality = value
            End Set
        End Property


        Public Property  CodePostal() As Integer
        Get
                Return _CodePostal
        End Get
            Set(value As Integer)
                _CodePostal =value
            End Set
        End Property

        Public  Property  Phone() As Integer    
        Get
                Return _Phone
        End Get
            Set(value As Integer)
                _Phone =value
            End Set
        End Property
#End Region

    End Class

End Namespace
