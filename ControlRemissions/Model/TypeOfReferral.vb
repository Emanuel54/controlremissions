﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace ControlRemissions.JimyDiaz.Model
    Public Class TypeOfReferral
        #Region "Campos"
        Private  _RemissionID As integer
        Private  _Explanetion As String
        Private _Amount as Decimal
        Private _Recurrence  As String
#End Region

        #Region "Propiedades"
        'Cambiar a Plural 
        Public Overridable Property Remissions() As ICollection(Of Remission)

        <Key()>
        Public Property RemissionID As Integer
        Get 
                Return _RemissionID
        End Get
            Set(value As Integer)
                _RemissionID = value
            End Set
        End Property

        Public Property Explanetion As String
        Get
                Return _Explanetion
        End Get
            Set(value As String)
                _Explanetion = value
            End Set
        End Property

    
        Public Property Amount As Decimal
            Get
                Return _Amount
            End Get
            Set(value As Decimal)
                _Amount = value
            End Set
        End Property

        

        Public Property Recurrence As String
            Get
                Return _Recurrence
            End Get
            Set(value as String)
                _Recurrence = value
            End Set
        End Property


#End Region
    End Class
End Namespace

