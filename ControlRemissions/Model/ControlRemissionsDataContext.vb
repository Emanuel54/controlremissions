﻿Imports System.Data.Entity
Imports System.Data.Entity.ModelConfiguration.Conventions
Imports  System.Data.SqlClient

Namespace ControlRemissions.JimyDiaz.Model
    Public Class ControlRemissionsDataContext
        Inherits DbContext
        Public  Property Agents() As DbSet(Of Agent)
        Public  Property Neighbors() As DbSet(Of Neighbor)
        Public  Property Remissions() As DbSet(Of Remission)
        Public  Property TypeOfReferrals() As DbSet(Of TypeOfReferral)
        Public  Property Vehicles() As DbSet(Of Vehicle)
    End Class

End Namespace