﻿Imports  System.ComponentModel.DataAnnotations
Imports  System.ComponentModel.DataAnnotations.Schema


Namespace ControlRemissions.JimyDiaz.Model
    Public Class Agent
        #Region "Campos"
        Private _AgentNumber As Integer
        Private _FirstName As String
        Private _LastName As String
        Private _Charge as String
        Private _Salary as String
        Private _Commissions As Decimal
#End Region

        #Region "Propiedades"

        Public Overridable Property Remissions() As ICollection(Of Remission)

        <Key()>
        Public Property AgentNumber() As Integer
        Get
                Return _AgentNumber
        End Get
            Set(value As Integer)
                _AgentNumber =value
            End Set
        End Property

  

        Public Property FirstName As String
            Get
                Return _FirstName
            End Get
            Set
                _FirstName = value
            End Set
        End Property

        Public Property LastName() As String
        Get
                Return _LastName
        End Get
            Set(value As String)
                _LastName = value
            End Set
        End Property

        Public  Property Charge() As String
        Get
                Return _Charge
        End Get
            Set(value As String)
                _Charge = value
            End Set
        End Property

 

        Public Property Salary As String
            Get
                Return _Salary
            End Get
            Set
                _Salary = value
            End Set
        End Property

        Public  Property Commissions() As Decimal    
        Get
                Return _Commissions
        End Get
            Set(value As Decimal)
                _Commissions = value
            End Set
        End Property
#End Region
End Class
    


End Namespace
