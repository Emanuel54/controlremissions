﻿Imports  System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema


Namespace  ControlRemissions.JimyDiaz.Model
    Public Class Vehicle
        #Region "Campos"
        Private _LicensePlate as String
        Private _NIT as String
        Private _Brand As String
        Private _Model As String
        Private _TypeOfVehicle As String
#End Region

        #Region "Propiedades"
        Public Overridable Property Neighbor() As Neighbor
        Public  Overridable Property Remissions() As ICollection(Of Remission)


        <Key()>
        Public Property LicensePlate as String
        Get
                Return _LicensePlate
        End Get
            Set(value as String)
                _LicensePlate = value
            End Set
        End Property
        <ForeignKey("Neighbor")>
        Public Property NIT As String
        Get
                Return _NIT
        End Get
            Set(value As String)
                _NIT = value
            End Set
        End Property

        Public Property Brand As String
        Get
                Return _Brand
        End Get
            Set(value As String)
                _Brand  =  value
            End Set
        End Property
        Public Property Model As String
        Get
                Return _Model
        End Get
            Set(value As String)
                _Model = value
            End Set
        End Property

        Public Property  TypeOfVehicle as  String
        Get
                Return _TypeOfVehicle
        End Get
            Set(value as  String)
                _TypeOfVehicle = value
            End Set
        End Property
#End Region

    End Class

End Namespace
