﻿Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports ControlRemissions.ControlRemissions.JimyDiaz.Model
Imports Tulpep.NotificationWindow

Public Class TypeOfReferralModelView
    Implements INotifyPropertyChanged,ICommand, IDataErrorInfo
    #Region "Campos"
    Private  _Explanetion as String
    Private _Amount as Decimal
    Private _Recurrence as String
    Private _Guardar As Boolean = False
    Private _Nuevo as Boolean = True
    Private _Actualizar as Boolean= True    
    Private _Eliminar as Boolean = True
    Private _Cancelar as Boolean = False    
    Private _Modelo as TypeOfReferralModelView
    Private DB as New ControlRemissionsDataContext
    Private _TypeOfReferral as TypeOfReferral
    Private _ListTypeOfReferral as New List(Of TypeOfReferral)
    Dim notificacion as New PopupNotifier


#End Region
    #Region "Propiedades"
    Public Property Explanetion() As String
    Get
            Return _Explanetion
    End Get
        Set(value As String)
            _Explanetion = value
            NotificarCambio("Explanetion")
        End Set
    End Property
    Public Property Amount() As Decimal
    Get
            Return _Amount
    End Get
        Set(value As Decimal)
            _Amount = value 
            NotificarCambio("Amount")
        End Set
    End Property
    Public Property Recurrence() As String
    Get
            Return _Recurrence
    End Get
        Set(value As String)
            _Recurrence = value
            NotificarCambio("Recurrence")
        End Set
    End Property
     Public  Property Guardar() As Boolean
    Get
            Return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar = value    
            NotificarCambio("Guardar")
        End Set
    End Property
          Public Property  Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public  Property Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value
            NotificarCambio("Actualizar")
        End Set
    End Property

    Public Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")

        End Set
    End Property
    Public Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property
    Public Property Modelo() As TypeOfReferralModelView
    Get
            Return _Modelo
    End Get
        Set(value As TypeOfReferralModelView)
            _Modelo = value
        End Set
    End Property
    Public Property ListTypeOfReferral() As List(Of TypeOfReferral)
    Get
            If _ListTypeOfReferral.Count = 0
                _ListTypeOfReferral = (From T in DB.TypeOfReferrals Select T).ToList()
            End If
            Return _ListTypeOfReferral
    End Get
        Set(value As List(Of TypeOfReferral))
            _ListTypeOfReferral = value
            NotificarCambio("ListTypeOfReferral")
        End Set
    End Property
    Public Property TypeOfReferral() As TypeOfReferral
    Get
            Return _TypeOfReferral
    End Get
        Set(value As TypeOfReferral)
            _TypeOfReferral = value  
            NotificarCambio("TypeOfReferral")
            If value IsNot Nothing
                Me.Explanetion = _TypeOfReferral.Explanetion
                Me.Amount = _TypeOfReferral.Amount
                Me.Recurrence = _TypeOfReferral.Recurrence
            End If
        End Set
    End Property
    Public  Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(me,New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public  Sub New ()
        Me.Modelo = Me
    End Sub

    Public Sub LimpiarControles()
        Me.Explanetion =""
        Me.Amount = 0
        Me.Recurrence = ""
        Me.TypeOfReferral= Nothing
    End Sub
#End Region

    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        If parameter = "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
            Me.Actualizar    = False
            Me.Eliminar = False
            Me.Cancelar = True
            LimpiarControles()
        End If

        If parameter = "guardar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Eliminar = True  
            Me.Actualizar = True
            Me.Cancelar = False
            Dim Nuevo as New TypeOfReferral
            Nuevo.Explanetion = Me.Explanetion
            Nuevo.Amount = Me.Amount
            Nuevo.Recurrence = Me.Recurrence
            DB.TypeOfReferrals.Add(Nuevo)
            DB.SaveChanges()
        '    MsgBox("Registro realizado")
            ListTypeOfReferral = (From T In DB.TypeOfReferrals  Select T).ToList()

             notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
           notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizado correctamente"
            notificacion.Popup()

        End If

        if parameter = "actualizar"
            If TypeOfReferral IsNot Nothing
                Me.TypeOfReferral.Explanetion = Me.Explanetion
                Me.TypeOfReferral.Amount = Me.Amount
                Me.TypeOfReferral.Recurrence = Me.Recurrence
                DB.Entry(TypeOfReferral).State = EntityState.Modified
                DB.SaveChanges()
                ListTypeOfReferral = (From T in DB.TypeOfReferrals Select T).ToList()
                
                 notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                notificacion.TitleText = "Registro Actualizado!"
                 notificacion.ContentText = "El registro se ha actualizado correctamente"
              notificacion.Popup()
            End If
        End If

        if parameter = "eliminar"
            Try
                       If TypeOfReferral isNot Nothing
                Dim Respuesta as MsgBoxResult = MsgBoxResult.No
                Respuesta = MsgBox("¿Desea eliminar este registro?",MsgBoxStyle.YesNo+MsgBoxStyle.Question+MsgBoxStyle.DefaultButton2,"Eliminar")
                if Respuesta = MsgBoxResult.Yes
                    DB.TypeOfReferrals.Remove(TypeOfReferral)
                    DB.SaveChanges()
                    ListTypeOfReferral =(From T In DB.TypeOfReferrals Select T).ToList()
                    LimpiarControles()
                         notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                   notificacion.TitleText = "Registro Eliminado!"
                     notificacion.ContentText = "El registro se ha eliminado correctamente"
                     notificacion.Popup()
                End If
                Else 
                    MsgBox("Debe de seleccionar un dato")
            End If
            Catch ex As Exception
                 Msgbox("Este registro tiene referencia a otras tablas",MsgBoxStyle.Exclamation,"Atención")
            End Try

        End If


        If parameter = "cancelar"
            Me.Nuevo  = True
            Me.Guardar = False
            Me.Actualizar = True
            Me.Eliminar = True
            Me.Cancelar = False
            LimpiarControles()
        End If
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
End Class
