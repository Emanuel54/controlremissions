﻿Imports System.ComponentModel
Imports  System.Data.Entity
Imports System.Drawing
Imports ControlRemissions.ControlRemissions.JimyDiaz.Model
Imports Tulpep.NotificationWindow

Public Class AgentModelView
    Implements INotifyPropertyChanged, IDataErrorInfo, ICommand
    #Region "Campos"
    Private _FirstName as String
    Private _LastName As String
    Private _Charge as String
    Private _Salary as String
    Private _Commissions as Decimal
    Private _ListAgents as New List(Of Agent)
    Private  DB as New ControlRemissionsDataContext
    Private  _Modelo as AgentModelView
    Private _Agent As Agent
    Private _Nuevo as Boolean = True    
    Private _Guardar as Boolean = False
    Private _Actualizar as Boolean = True
    Private _Eliminar as Boolean =True
    Private _Cancelar as Boolean = False
    Dim notificacion as New PopupNotifier
#End Region
    #Region "Propiedades"
    Public Property FirstName() As String
    Get
            Return _FirstName
    End Get
        Set(value As String)
            _FirstName = value
            NotificarCambio("FirstName")
        End Set
    End Property
    Public Property LastName() As String
    Get
            Return _LastName
    End Get
        Set(value As String)
            _LastName = value
            NotificarCambio("LastName")
        End Set
    End Property
   Public Property Charge As String
    Get
           Return _Charge
    End Get
       Set(value As String)
            _Charge = value 
            NotificarCambio("Charge")
       End Set
   End Property
    Public Property Salary() As String
    Get
            Return  _Salary
    End Get
        Set(value As String)
            _Salary = value
            NotificarCambio("Salary")
        End Set
    End Property
    Public Property Commnissions() As Decimal
    Get
            Return _Commissions
    End Get
        Set(value As Decimal)
            _Commissions = value
            NotificarCambio("Commnissions")
        End Set
    End Property

    Public  Property ListAgents() As List(Of Agent)
    Get
            If _ListAgents.Count = 0
                _ListAgents = (From A in DB.Agents Select A).ToList()
            End If
            Return _ListAgents
    End Get
        Set(value As List(Of Agent))
            _ListAgents = value
            NotificarCambio("ListAgents")
        End Set
    End Property


    Public Property  Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public  Property Guardar() As Boolean
    Get
            Return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar = value    
            NotificarCambio("Guardar")
        End Set
    End Property
    Public  Property Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value
            NotificarCambio("Actualizar")
        End Set
    End Property

    Public Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")

        End Set
    End Property
    Public Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property

    Public Property Modelo() As AgentModelView
    Get
            Return _Modelo
    End Get
        Set(value As AgentModelView)
            _Modelo = value 
             NotificarCambio("Modelo")
        End Set
    End Property

    Public Property Agent() As Agent
    Get
            Return _Agent
    End Get
        Set(value As Agent)
            _Agent = value
            NotificarCambio("Agent")
            If value IsNot Nothing
                Me.FirstName = _Agent.FirstName
                Me.LastName = _Agent.LastName
                Me.Charge = _Agent.Charge
                Me.Salary = _Agent.Salary
                Me.Commnissions = _Agent.Commissions
            End If
        End Set
    End Property

    Public  Sub LimpiarControles()
        Me.FirstName = ""
        Me.LastName = ""
        Me.Charge = ""
        Me.Salary =""
        Me.Commnissions = 0
        Me.Agent = Nothing
    End Sub
    Public Sub NotificarCambio(ByVal Propiedad  As String)
        RaiseEvent PropertyChanged(Me,New PropertyChangedEventArgs(Propiedad))
    End Sub
    Public Sub New ()
        Me.Modelo = Me
    End Sub
#End Region

    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        If parameter = "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
            Me.Actualizar = False
            Me.Eliminar = False
            Me.Cancelar = True
            LimpiarControles()
        End If

        If parameter = "guardar"
            Me.Nuevo = True  
            Me.Guardar = False
            Me.Actualizar = True    
            Me.Eliminar = True  
            Me.Cancelar = False
            Dim Nuevo as New Agent
            Nuevo.FirstName = Me.FirstName
            Nuevo.LastName = Me.LastName
            Nuevo.Charge = Me.Charge
            Nuevo.Salary = Me.Salary
            Nuevo.Commissions = Me.Commnissions
            DB.Agents.Add(Nuevo)
            Db.SaveChanges()
            ListAgents = (From A In DB.Agents Select A).ToList()
            LimpiarControles()
             notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
             notificacion.TitleText = "Registro Realizado!"
             notificacion.ContentText = "El registro se ha realizado correctamente"
             notificacion.Popup()
        End If

        If parameter = "eliminar"
            Try 
                 If Agent IsNot Nothing
                Dim Respuesta as MsgBoxResult = MsgBoxResult.No
                Respuesta  = MsgBox("¿Esta seguro de eliminar el registro?",MsgBoxStyle.Question+MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2,"Eliminar")
                If Respuesta = MsgBoxResult.Yes
                    DB.Agents.Remove(Agent)
                    DB.SaveChanges()
                        LimpiarControles()
                    ListAgents = (From A In DB.Agents Select A).ToList()
                     notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                    notificacion.TitleText = "Registro Eliminado!"
                    notificacion.ContentText = "El registro se ha eliminado correctamente"
                    notificacion.Popup()
                End If
                
                Else 
                    MsgBox("Seleccione algun elemento")   

            End If
            Catch ex As Exception
                Msgbox("Este registro tiene referencia a otras tablas",MsgBoxStyle.Exclamation,"Atención")
            End Try
           
        End If

        if parameter = "actualizar"
            if Agent IsNot Nothing
                Me.Agent.FirstName = Me.FirstName
                Me.Agent.LastName = Me.LastName
                Me.Agent.Charge = Me.Charge
                Me.Agent.Salary = Me.Salary
                Me.Agent.Commissions = Me.Commnissions
                DB.Entry(Agent).State = EntityState.Modified
                DB.SaveChanges()
                ListAgents = (From A In DB.Agents Select A).ToList()
                 notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                    notificacion.TitleText = "Registro Actualizado!"
                    notificacion.ContentText = "El registro se ha actualizado correctamente"
                    notificacion.Popup()
            End If
        End If


        If parameter="cancelar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Actualizar = True
            Me.Eliminar = True
            Me.Cancelar = False
            LimpiarControles()
        End If

    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
End Class
