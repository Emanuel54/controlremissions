﻿Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports ControlRemissions.ControlRemissions.JimyDiaz.Model
Imports Tulpep.NotificationWindow


Public Class RemissionModelView
    Implements INotifyPropertyChanged, IDataErrorInfo, ICommand
#Region "Campos"
    Private _DateOfSubmission As Date
    Private _Direction As String
    Private _TypeOfReferral As TypeOfReferral
    Private _Agent As Agent
    Private _Vehicle As Vehicle
    Private DB As New ControlRemissionsDataContext
    Private _Modelo As RemissionModelView
    Private _Remission As Remission
    Private _ListTypeOfRemission As New List(Of TypeOfReferral)
    Private _ListAgents As New List(Of Agent)
    Private _ListVehicles As New List(Of Vehicle)
    Private _ListRemissions As New List(Of Remission)
    Private _Nuevo As Boolean = True
    Private _Guardar As Boolean = False
    Private _Actualizar As Boolean = True
    Private _Eliminar As Boolean = True
    Private _Cancelar As Boolean = False
     Dim notificacion as New PopupNotifier
    


#End Region
#Region "Propiedades"
    Public Property DateOfSubmission() As Date
        Get

            Return _DateOfSubmission
        End Get
        Set(value As Date)
            _DateOfSubmission = value
            NotificarCambio("DateOfSubmission")
        End Set
    End Property
    Public Property Direction() As String
        Get
            Return _Direction
        End Get
        Set(value As String)
            _Direction = value
            NotificarCambio("Direction")
        End Set
    End Property
    Public Property TypeOfReferral() As TypeOfReferral
        Get
            Return _TypeOfReferral
        End Get
        Set(value As TypeOfReferral)
            _TypeOfReferral = value
            NotificarCambio("TypeOfReferral")
        End Set
    End Property
    Public Property Agent() As Agent
        Get
            Return _Agent
        End Get
        Set(value As Agent)
            _Agent = value
            NotificarCambio("Agent")
        End Set
    End Property
    Public Property Vehicle() As Vehicle
        Get
            Return _Vehicle
        End Get
        Set(value As Vehicle)
            _Vehicle = value
            NotificarCambio("Vehicle")
        End Set
    End Property
    Public Property Modelo() As RemissionModelView
        Get
            Return _Modelo
        End Get
        Set(value As RemissionModelView)
            _Modelo = value
            NotificarCambio("Modelo")
        End Set
    End Property
    Public Property Remission() As Remission
        Get
            Return _Remission
        End Get
        Set(value As Remission)
            _Remission = value
            NotificarCambio("Remission")
            If value IsNot Nothing
                Me.DateOfSubmission = _Remission.DateOfSubmission
                Me.Direction = _Remission.Direction
                Me.TypeOfReferral = _Remission.TypeOfReferral
                Me.Agent = _Remission.Agent
                Me.Vehicle = _Remission.Vehicle
            End If

        End Set
    End Property
    Public Property ListTypeOfRemission() As List(Of TypeOfReferral)
        Get
            If _ListTypeOfRemission.Count = 0
                _ListTypeOfRemission = (From T In DB.TypeOfReferrals Select T).ToList()
            End If
            Return _ListTypeOfRemission
        End Get
        Set(value As List(Of TypeOfReferral))
            _ListTypeOfRemission = value
            NotificarCambio("ListTypeOfRemission")
        End Set
    End Property
    Public Property ListAgents() As List(Of Agent)
        Get
            If _ListAgents.Count = 0
                _ListAgents = (From A In DB.Agents Select A).ToList()
            End If
            Return _ListAgents
        End Get
        Set(value As List(Of Agent))
            _ListAgents = value
            NotificarCambio("ListAgents")
        End Set
    End Property
    Public Property ListVehicles As List(Of Vehicle)
        Get
            If _ListVehicles.Count = 0
                _ListVehicles = (From V In DB.Vehicles Select V).ToList()
            End If
            Return _ListVehicles
        End Get
        Set(value As List(Of Vehicle))
            _ListVehicles = value
            NotificarCambio("ListVehicles")
        End Set
    End Property
    Public Property ListRemissions() As List(Of Remission)
        Get
            If _ListRemissions.Count = 0
                _ListRemissions = (From R In DB.Remissions Select R).ToList()
            End If
            Return _ListRemissions
        End Get
        Set(value As List(Of Remission))
            _ListRemissions = value
            NotificarCambio("ListRemissions")
        End Set
    End Property
    Public Property Guardar() As Boolean
        Get
            Return _Guardar
        End Get
        Set(value As Boolean)
            _Guardar = value
            NotificarCambio("Guardar")
        End Set
    End Property
    Public Property Nuevo() As Boolean
        Get
            Return _Nuevo
        End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public Property Actualizar() As Boolean
        Get
            Return _Actualizar
        End Get
        Set(value As Boolean)
            _Actualizar = value
            NotificarCambio("Actualizar")
        End Set
    End Property

    Public Property Eliminar() As Boolean
        Get
            Return _Eliminar
        End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")

        End Set
    End Property
    Public Property Cancelar() As Boolean
        Get
            Return _Cancelar
        End Get
        Set(value As Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property
    Public Sub New()
        Me.Modelo = Me
        Me.DateOfSubmission = Date.Now
    End Sub
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Sub LimpiarControles
        Me.DateOfSubmission = Date.Now
        Me.Direction = ""
        Me.TypeOfReferral = Nothing
        Me.Agent = Nothing
        Me.Vehicle = Nothing
        Me.Remission = Nothing
    End Sub






#End Region
#Region "Eventos"




#End Region
    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Async Sub Execute(parameter As Object) Implements ICommand.Execute
        If parameter = "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
            Me.Actualizar = False
            Me.Eliminar = False
            Me.Cancelar = True
            LimpiarControles()
        End If

        If parameter = "guardar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Actualizar = True
            Me.Eliminar = True
            Me.Cancelar = False
            Dim Nuevo As New Remission
            Nuevo.DateOfSubmission = Me.DateOfSubmission
            Nuevo.Direction = Me.Direction
            Nuevo.TypeOfReferral = Me.TypeOfReferral
            Nuevo.Agent = Me.Agent
            Nuevo.Vehicle = Me.Vehicle
            DB.Remissions.Add(Nuevo)
            DB.SaveChanges()
          
            notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizado correctamente"
            notificacion.Popup()

            



            ListRemissions = (From R In DB.Remissions Select R).ToList()
        End If

        If parameter = "actualizar"
            If Agent IsNot Nothing
                Me.Remission.DateOfSubmission = DateOfSubmission
                Me.Remission.Direction = Me.Direction
                Me.Remission.TypeOfReferral = Me.TypeOfReferral
                Me.Remission.Agent = Me.Agent
                Me.Remission.Vehicle = Me.Vehicle
                DB.Entry(Agent).State = EntityState.Modified
                DB.SaveChanges()
                ListRemissions = (From R In DB.Remissions Select R).ToList()
             '   Dim notificacion as New PopupNotifier
                 notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                notificacion.TitleText = "Registro Actualizado!"
                 notificacion.ContentText = "El registro se ha actualizado correctamente"
                  notificacion.Popup()
            End If
        End If

        If parameter = "eliminar"
            If Remission IsNot Nothing
                Dim Registro As MsgBoxResult = MsgBoxResult.No
                Registro = MsgBox("¿Desea elimnar este registro?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Eliminar")
                If Registro = MsgBoxResult.Yes
                    DB.Remissions.Remove(Remission)
                    DB.SaveChanges()
                    ListRemissions = (From R In DB.Remissions Select R).ToList()
                    LimpiarControles()
                 '   Dim notificacion as New PopupNotifier
                     notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                     notificacion.TitleText = "Registro Eliminado!"
                      notificacion.ContentText = "El registro se ha eliminado correctamente"
                       notificacion.Popup()
                End If
            Else
                MsgBox("Debe de seleccionar un dato")
            End If
        End If

        If parameter = "cancelar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Actualizar = True
            Me.Eliminar = True
            Me.Cancelar = False
            LimpiarControles()
        End If
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
End Class
