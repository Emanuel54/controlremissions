﻿Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports ControlRemissions.ControlRemissions.JimyDiaz.Model
Imports Tulpep.NotificationWindow

Public Class VehicleModelView
    Implements INotifyPropertyChanged, IDataErrorInfo, ICommand
    #Region "Campos"
    Private _LicensePLate as String
    Private _NIT As String
    Private _Brand as String
    Private _Model As String
    Private _TypeOfVehicle as String
    Private _Nuevo as Boolean = True    
    Private _Guardar as Boolean = False
    Private _Actualizar as Boolean = True
    Private _Eliminar as Boolean =True
    Private _Cancelar as Boolean = False
    Private DB As New ControlRemissionsDataContext
    Private _Modelo As VehicleModelView
    Private _Vehicle As Vehicle
    Private _Neighbor as Neighbor
    Private _ListNeighbors As New List(Of Neighbor)
    Private _ListVehicles as New List(Of Vehicle)
    Dim notificacion as New PopupNotifier
#End Region
    #Region "Propiedades"
    Public Property LicensePlate() As String
    Get
            Return _LicensePLate
    End Get
        Set(value As String)
            _LicensePLate = value
            NotificarCambio("LicensePlate")
        End Set
    End Property
    Public Property NIT As String
    Get
            Return _NIT
    End Get
        Set(value As String)
            _NIT = value
            NotificarCambio("NIT")
        End Set
    End Property
    Public Property Brand() As String
    Get
            Return _Brand
    End Get
        Set(value As String)
            _Brand = value
            NotificarCambio("Brand")
        End Set
    End Property

    Public Property Model() As String
    Get
            Return   _Model
    End Get
        Set(value As String)
            _Model   = value
            NotificarCambio("Model")
        End Set
    End Property
    Public Property TypeOfVehicle() As String
    Get
            Return _TypeOfVehicle
    End Get
        Set(value As String)
            _TypeOfVehicle = value
            NotificarCambio("TypeOfVehicle")
        End Set
    End Property
 

        Public Property  Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public  Property Guardar() As Boolean
    Get
            Return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar = value    
            NotificarCambio("Guardar")
        End Set
    End Property
    Public  Property Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value
            NotificarCambio("Actualizar")
        End Set
    End Property

    Public Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")

        End Set
    End Property
    Public Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property

    Public Property Modelo() As VehicleModelView
    Get
            Return _Modelo
    End Get
        Set(value As VehicleModelView)
            _Modelo  = value
            NotificarCambio("Modelo")
        End Set
    End Property
    Public Property Vehicle as Vehicle
    Get
            Return _Vehicle
    End Get
        Set(value as Vehicle)
            _Vehicle = value
            NotificarCambio("Vehicle")
            if value IsNot Nothing
                     Me.LicensePlate = _Vehicle.LicensePlate
              
            Me.Neighbor = _Vehicle.Neighbor
            Me.Brand = _Vehicle.Brand
            Me.Model = _Vehicle.Model
            Me.TypeOfVehicle = _Vehicle.TypeOfVehicle
            End If
       
        End Set
    End Property

    Public Property Neighbor() As Neighbor
    Get
            Return _Neighbor
    End Get
        Set(value As Neighbor)
            _Neighbor = value
            NotificarCambio("Neighbor")
        End Set
    End Property
    Public Property ListVehicles() As List(Of Vehicle)
    Get
            If _ListVehicles.Count = 0
                _ListVehicles = (From V in DB.Vehicles Select V).ToList()
            End If
            Return _ListVehicles
    End Get
        Set(value As List(Of Vehicle))
            _ListVehicles = value
            NotificarCambio("ListVehicles")
        End Set
    End Property
    Public Property ListNeighbors() As List(Of Neighbor)
    Get
            If _ListNeighbors.Count = 0
                _ListNeighbors  =(From N In DB.Neighbors Select N).ToList() 
            End If
            Return _ListNeighbors
    End Get
        Set(value As List(Of Neighbor))
            _ListNeighbors = value
            NotificarCambio("ListNeighbors")
        End Set
    End Property

    Public Sub NotificarCambio(ByVal Propiedad As String  )
        RaiseEvent PropertyChanged(Me,New  PropertyChangedEventArgs(Propiedad))
    End Sub
    Public Sub New ()
        Me.Modelo = Me
    End Sub

    Public  Sub LimpiarControles()
        Me.LicensePlate = ""
        Me.Neighbor = Nothing
        Me.Brand = ""
        Me.Model = ""
        Me.TypeOfVehicle = ""
        Me.Vehicle = Nothing
    End Sub
#End Region
    
    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        If parameter = "nuevo"
            Nuevo = False
            Guardar = True  
            Actualizar = False
            Eliminar = False
            Cancelar = True
            LimpiarControles()
        End If

        If parameter = "guardar"
           Me.Nuevo = true
            Guardar = false
            Eliminar = True
            Actualizar = True
            Cancelar = False

            Dim Nuevo as New Vehicle
            Nuevo.LicensePlate = me.LicensePlate
            Nuevo.Neighbor =Me.Neighbor
            Nuevo.Brand = Me.Brand
            Nuevo.Model = Me.Model
            Nuevo.TypeOfVehicle = Me.TypeOfVehicle
            DB.Vehicles.Add(Nuevo)
            DB.SaveChanges()
            ListVehicles = (From V In DB.Vehicles Select V).ToList()
             notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizado correctamente"
            notificacion.Popup()

        End If

        If parameter = "eliminar"
            Try 
                
            If Vehicle IsNot Nothing
                Dim Respuesta as MsgBoxResult = MsgBoxResult.No
                Respuesta = Msgbox("¿Desea eliminar este registro?", MsgBoxStyle.Question+MsgBoxStyle.YesNo+MsgBoxStyle.DefaultButton2,"Eliminar")
                If Respuesta = MsgBoxResult.Yes
                    DB.Vehicles.Remove(Vehicle)
                    DB.SaveChanges()
                    ListVehicles =(From V in DB.Vehicles  Select V).ToList()           
                    LimpiarControles()         
                     notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                    notificacion.TitleText = "Registro Eliminado!"
                    notificacion.ContentText = "El registro se ha eliminado correctamente"
                    notificacion.Popup()

                End If
                Else 
                MsgBox("Debe de seleccionar un elemento")
            End If
            Catch ex As Exception
                Msgbox("Este registro tiene referencia a otras tablas",MsgBoxStyle.Exclamation,"Atención")
            End Try

        End If

        If parameter = "actualizar"
            if Vehicle IsNot Nothing
                Me.Vehicle.Neighbor = Me.Neighbor
                Me.Vehicle.Brand = Me.Brand
                Me.Vehicle.Model = Me.Model
                Me.Vehicle.TypeOfVehicle = Me.TypeOfVehicle
                DB.Entry(Vehicle).State = EntityState.Modified
                Db.SaveChanges()
                ListVehicles = (From V in DB.Vehicles  Select V).ToList()
                notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                notificacion.TitleText = "Registro Actulizado!"
                notificacion.ContentText = "El registro se ha actualizado correctamente"
                notificacion.Popup()

            End If
        End If

        if parameter = "cancelar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Actualizar = True    
            Me.Eliminar = True
            Me.Cancelar = False
            LimpiarControles()
        End If
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
End Class
