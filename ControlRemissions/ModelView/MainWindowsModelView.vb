﻿Imports System.ComponentModel
Public Class MainWindowsModelView
    Implements INotifyPropertyChanged, ICommand
    #Region "Campos"
    Private _Modelo as MainWindowsModelView

#End Region

    #Region "Propiedades"
    PUblic Property  Modelo() As MainWindowsModelView
    Get
            Return _Modelo
    End Get
        Set(value As MainWindowsModelView)
            _Modelo = value
        End Set
    End Property
#End Region
    
    Public Sub New ()
        Me.Modelo = Me
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        if parameter= "Agent"
            Dim WindowsAgent As New AgentView
            WindowsAgent.ShowDialog()
        End If

        if parameter = "Neighbor"
            Dim WindowsNeighbor As New NeighborView
            WindowsNeighbor.ShowDialog()
        End If

        if parameter = "Remission"
            Dim WindowsRemission as New RemissionView
            WindowsRemission.ShowDialog()
        End If

        if parameter ="TypeOfReferral"
            Dim WindowsTypeOfreferral As New TypeOfReferralView
            WindowsTypeOfreferral.ShowDialog()
        End If

        if parameter = "Vehicle"
            Dim WindowsVehicle as New VehicleView
            WindowsVehicle.ShowDialog()
        End If
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
End Class
