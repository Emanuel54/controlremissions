﻿Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports ControlRemissions.ControlRemissions.JimyDiaz.Model
Imports Tulpep.NotificationWindow

Public Class NeighborModelView
    Implements INotifyPropertyChanged,ICommand,IDataErrorInfo
    #Region "Campos"
    Private _NIT as String
    Private _DPI as Long
    Private _FirstName as String
    Private _LastName as String
    Private _Direction as String
    Private _Municipality As String
    Private _CodePostal as Integer
    Private _Phone as Integer
    Private DB as New ControlRemissionsDataContext
    Private _ListNeighbors as New List(Of Neighbor)
    Private _Modelo As NeighborModelView
    Private _Neighbor As Neighbor
     Private _Nuevo as Boolean = True    
    Private _Guardar as Boolean = False
    Private _Actualizar as Boolean = True
    Private _Eliminar as Boolean =True
    Private _Cancelar as Boolean = False
    Dim notificacion As New PopupNotifier
    
#End Region

#Region "Propiedades"
    Public Property NIT() As String
    Get
            Return _NIT
    End Get
        Set(value As String)
            _NIT = value    
            NotificarCambio("NIT")
        End Set
    End Property
    Public  Property DPI() As Long
    Get
            Return _DPI
    End Get
        Set(value As Long)
            _DPI = value
            NotificarCambio("DPI")
        End Set
    End Property
    Public Property FirstName() As String
    Get
            Return _FirstName
    End Get
        Set(value As String)
            _FirstName = value  
            NotificarCambio("FirstName")
        End Set
    End Property
    Public Property  LastName() As String
    Get
            Return _LastName
    End Get
        Set(value As String)
            _LastName = value
            NotificarCambio("LastName")
        End Set
    End Property
    Public Property Direction() As String
    Get
            Return _Direction
    End Get
        Set(value As String)
            _Direction = value
            NotificarCambio("Direction")
        End Set
    End Property
    Public Property Municipality() As String
    Get
            Return  _Municipality
    End Get
        Set(value As String)
            _Municipality = value   
            NotificarCambio("Municipality")
        End Set
    End Property
    Public Property CodePostal() As Integer 
    Get
            Return _CodePostal
    End Get
        Set(value As Integer)
            _CodePostal = value 
            NotificarCambio("CodePostal")
        End Set
    End Property
    Public Property Phone() As Integer
    Get
            Return _Phone
    End Get
        Set(value As Integer)
            _Phone = value
            NotificarCambio("Phone")
        End Set
    End Property

    Public Property ListNeighbors() As List(Of Neighbor)
    Get
            If _ListNeighbors.Count = 0
                _ListNeighbors = (From N In DB.Neighbors Select N).ToList()

            End If
            Return _ListNeighbors
    End Get
        Set(value As List(Of Neighbor))
            _ListNeighbors = value
            NotificarCambio("ListNeighbors")
        End Set
    End Property

    Public Property  Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public  Property Guardar() As Boolean
    Get
            Return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar = value    
            NotificarCambio("Guardar")
        End Set
    End Property
    Public  Property Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value
            NotificarCambio("Actualizar")
        End Set
    End Property

    Public Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")

        End Set
    End Property
    Public Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property
    Public Property Modelo As NeighborModelView
    Get

            Return _Modelo
    End Get
        Set(value As NeighborModelView)
            _Modelo =value  
            NotificarCambio("Modelo")
        End Set
    End Property
    Public Property Neighbor() As Neighbor
    Get
            Return _Neighbor
    End Get
        Set(value As Neighbor)
            _Neighbor = value   
            NotificarCambio("Neighbor")
            If value IsNot Nothing
                Me.NIT = _Neighbor.NIT
                Me.DPI = _Neighbor.DPI
                Me.FirstName = _Neighbor.FirstName
                Me.LastName = _Neighbor.LastName
                Me.Direction = _Neighbor.Direction
                Me.Municipality = _Neighbor.Municipality
                Me.CodePostal = _Neighbor.CodePostal
                Me.Phone = _Neighbor.Phone
            End If
        End Set
    End Property

    Public  Sub New ()
        Me.Modelo = Me
    End Sub

    Public  Sub NotificarCambio(ByVal  Propiedad As String)
        RaiseEvent PropertyChanged  (Me,New  PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Sub LimpiarControles
        Me.NIT = ""
        Me.DPI = 0
        Me.FirstName = ""
        Me.LastName = ""
        Me.Direction = ""
        Me.Municipality = ""
        Me.CodePostal = 0
        Me.Phone = 0
        Me.Neighbor = Nothing
    End Sub

#End Region
    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        if parameter = "nuevo"
            Nuevo = False
            Guardar = True  
            Actualizar = False
            Eliminar = false
            Cancelar = True 
            LimpiarControles()
        End If

        if parameter = "guardar"
                  Me.Nuevo = True  
            Me.Guardar = False
            Me.Actualizar = True    
            Me.Eliminar = True  
            Me.Cancelar = False
            Dim Nuevo as New Neighbor
            Nuevo.NIT = Me.NIT
            Nuevo.DPI = Me.DPI
            Nuevo.FirstName = Me.FirstName
            Nuevo.LastName = Me.LastName
            Nuevo.Direction = Me.Direction
            Nuevo.Municipality = Me.Municipality
            Nuevo.CodePostal =  Me.CodePostal
            Nuevo.Phone = Me.Phone
            DB.Neighbors.Add(Nuevo)
            DB.SaveChanges()
            ListNeighbors = (From N in DB.Neighbors Select N).ToList()
             notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
           notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizado correctamente"
            notificacion.Popup()
        End If

        if parameter = "eliminar"

            Try 
                   if Neighbor IsNot Nothing
                Dim Respuesta as MsgBoxResult = MsgBoxResult.No
                Respuesta  = MsgBox("¿Esta seguro de eliminar el registro?",MsgBoxStyle.Question+MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2,"Eliminar")
                If Respuesta = MsgBoxResult.Yes
                    DB.Neighbors.Remove(Neighbor)
                    Db.SaveChanges()
                    ListNeighbors = (From N in DB.Neighbors Select N).ToList()
                    LimpiarControles()
                    notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                    notificacion.TitleText = "Registro Eliminado!"
                    notificacion.ContentText = "El registro se ha eliminado correctamente"
                    notificacion.Popup()
                End If
                    Else 
                    MsgBox("Seleccione algun elemento")   
            End If
            Catch ex As Exception
                 Msgbox("Este registro tiene referencia a otras tablas",MsgBoxStyle.Exclamation,"Atención")
            End Try
         
        End If


        if parameter = "actualizar"
            If Neighbor IsNot Nothing
                Neighbor.DPI = mE.DPI
                Me.Neighbor.FirstName = Me.FirstName
                Me.Neighbor.LastName = Me.LastName
                Me.Neighbor.Direction = Me.Direction
                Me.Neighbor.Municipality = Me.Municipality
                Me.Neighbor.CodePostal = Me.CodePostal
                Me.Neighbor.Phone = Me.Phone
                DB.Entry(Neighbor).State = EntityState.Modified
                Db.SaveChanges()
                ListNeighbors = (From N in DB.Neighbors Select N).ToList()
                notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                notificacion.TitleText = "Registro Actualizado!"
                notificacion.ContentText = "El registro se ha actualizado correctamente"
                notificacion.Popup()
            End If
        End If


        If parameter = "cancelar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Actualizar = True    
            Me.Eliminar = True  
            Me.Cancelar = False
            LimpiarControles()
        End If
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
End Class
